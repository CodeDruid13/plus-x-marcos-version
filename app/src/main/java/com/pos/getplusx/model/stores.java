package com.pos.getplusx.model;

public class stores {


    private String business_name,address,city,logo;

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public stores(String business_name, String address, String city, String logo) {
        this.business_name = business_name;
        this.address = address;
        this.city = city;
        this.logo = logo;
    }

    public stores() {

        }

}
