package com.pos.getplusx.model;

public class items {

    private String  Product_Image,Product_Name,Product_Price,Stock_Quantity,quantity;

    public items(){

    }

    public String getProduct_Image() {
        return Product_Image;
    }

    public void setProduct_Image(String product_Image) {
        Product_Image = product_Image;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getProduct_Price() {
        return Product_Price;
    }

    public void setProduct_Price(String product_Price) {
        Product_Price = product_Price;
    }

    public String getStock_Quantity() {
        return Stock_Quantity;
    }

    public void setStock_Quantity(String stock_Quantity) {
        Stock_Quantity = stock_Quantity;
    }

    public items(String product_Image, String product_Name, String product_Price, String stock_Quantity, String Quantity) {
        Product_Image = product_Image;
        Product_Name = product_Name;
        Product_Price = product_Price;
        Stock_Quantity = stock_Quantity;
        quantity = Quantity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
