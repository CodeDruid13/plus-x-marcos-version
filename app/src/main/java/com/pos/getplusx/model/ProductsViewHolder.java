package com.pos.getplusx.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pos.getplusx.R;
import com.squareup.picasso.Picasso;

public class ProductsViewHolder extends RecyclerView.ViewHolder {

    View mView;

    private TextView stkk,ppname,pprc,iimg;

    public ImageView imageView;


    public ProductsViewHolder(View itemView) {
        super(itemView);

        mView = itemView;

        stkk = mView.findViewById(R.id.stock);
        ppname = mView.findViewById(R.id.p_name);
        pprc = mView.findViewById(R.id.price);
        imageView = mView.findViewById(R.id.productimage);

    }


    public void setStk(String stk){
        stkk.setText(stk);
    }

    public void setPpname(String pname) {
        ppname.setText(pname);
    }


    public void setPprc(String prc) {
        pprc.setText(prc);
    }

    public void setImageView(String image)
    {
        Picasso.get().load(image).into(imageView);
    }

}