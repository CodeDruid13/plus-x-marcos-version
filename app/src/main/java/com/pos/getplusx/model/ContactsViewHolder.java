package com.pos.getplusx.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pos.getplusx.R;
import com.squareup.picasso.Picasso;

public class ContactsViewHolder extends RecyclerView.ViewHolder {
    View mView;

    private TextView stkk,ppname,pprc,iimg;

    public ImageView imageView;


    public ContactsViewHolder(View itemView) {
        super(itemView);

        mView = itemView;

        stkk = mView.findViewById(R.id.l_name);
        ppname = mView.findViewById(R.id.f_name);
        pprc = mView.findViewById(R.id.phone_number);
        imageView = mView.findViewById(R.id.city);

    }

    public void setStkk(TextView stkk) {
        this.stkk = stkk;
    }

    public void setPpname(TextView ppname) {
        this.ppname = ppname;
    }

    public void setPprc(TextView pprc) {
        this.pprc = pprc;
    }
}