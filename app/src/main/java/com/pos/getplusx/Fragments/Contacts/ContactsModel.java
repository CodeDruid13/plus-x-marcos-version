package com.pos.getplusx.Fragments.Contacts;

public class ContactsModel {

    private String First_Name,Last_Name,Phone_Number,City,Email;

    public ContactsModel() {
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public ContactsModel(String email) {

        Email = email;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public String getPhone_Number() {
        return Phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        Phone_Number = phone_Number;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public ContactsModel(String first_Name, String last_Name, String phone_Number, String city) {
        First_Name = first_Name;
        Last_Name = last_Name;
        Phone_Number = phone_Number;
        City = city;
    }
}
