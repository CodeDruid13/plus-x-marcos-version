package com.pos.getplusx.Fragments.Contacts;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.pos.getplusx.R;

public class ContactsViewHolder extends RecyclerView.ViewHolder {
    View mView;

    private TextView fname,lname,pnum,iimg,city,emaile;

    public ImageView imageView,letter;

    ColorGenerator generator = ColorGenerator.MATERIAL;

    public ContactsViewHolder(View itemView) {
        super(itemView);

        mView = itemView;

        fname = mView.findViewById(R.id.f_name);
        lname = mView.findViewById(R.id.l_name);
        pnum = mView.findViewById(R.id.phone_number);
        emaile = mView.findViewById(R.id.city);

       // letter = mView.findViewById(R.id.gmail_item_letter);

    }

    public void setFname(String fnamio) {
        fname.setText(fnamio);
    }

    public void setLname(String lnamio) {
        lname.setText(lnamio);
    }

    public void setPnum(String pnumio) {
        pnum.setText(pnumio);
    }

    public void setEmail(String emailio) {
        emaile.setText(emailio);
    }
}