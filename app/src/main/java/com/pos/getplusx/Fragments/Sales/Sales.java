package com.pos.getplusx.Fragments.Sales;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pos.getplusx.Activities.Preview_Checkout.Preview_items;
import com.pos.getplusx.Activities.Preview_Checkout.dialogBasicTags;
import com.pos.getplusx.Fragments.Home.Home;
import com.pos.getplusx.R;
import com.pos.getplusx.model.items;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import spencerstudios.com.bungeelib.Bungee;

import static android.widget.LinearLayout.VERTICAL;


/**
 * A simple {@link Fragment} subclass.
 */
public class Sales extends Fragment {

    FirebaseRecyclerAdapter adapter;
    DatabaseReference myref;
    TextView items,total;
    Button checkout;

    ArrayList<String> items_ids = new ArrayList<>();

    String test,Fname,Lname,email,pnum,a;
    String totalamount;

    CardView list;
    private boolean  mProcessFav = false;


    public Sales() {
        // Required empty public constructor
    }

    View view;
    FloatingActionButton refresh;
    int counter = 0;
    int sum = 0;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sales, container, false);

        final Bundle bundle = this.getArguments();
        assert bundle != null;
        test = bundle.getString("id");
        a = bundle.getString("user_key");
        Fname = bundle.getString("first_name");
        Lname = bundle.getString("last_name");
        email = bundle.getString("email");
        pnum = bundle.getString("phone_number");


        items = view.findViewById(R.id.item_counter);
        total = view.findViewById(R.id.totalcharge);
        list = view.findViewById(R.id.preview);

        checkout = view.findViewById(R.id.checkout);
//        proceed = view.findViewById(R.id.)




        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        myref = FirebaseDatabase.getInstance().getReference().child("Businesses").child(uid).child(test).child("Products");

        myref.keepSynced(true);

        RecyclerView recyclerView = view.findViewById(R.id.items);
//        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration decoration = new DividerItemDecoration(getActivity(), VERTICAL);
        recyclerView.addItemDecoration(decoration);

        FirebaseRecyclerOptions<items> options =
                new FirebaseRecyclerOptions.Builder<items>()
                        .setQuery(myref, items.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<items, itemsViewHolder>(options) {
            @NonNull
            @Override
            public itemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View s = LayoutInflater.from(getActivity())
                        .inflate(R.layout.item_product, parent, false);

                return new itemsViewHolder(s);
            }

            @Override
            protected void onBindViewHolder(@NonNull itemsViewHolder holder, final int position, @NonNull final items model) {
                final String item_id = getRef(position).getKey();

                holder.setStk(model.getStock_Quantity());
                holder.setPpname(model.getProduct_Name());
                holder.setPprc(model.getProduct_Price());
                holder.setImageView(model.getProduct_Image());
//
//                holder.itemView.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View view) {
//                        sum = sum+ Integer.parseInt(model.getProduct_Price());
//                        total.setText(String.valueOf(sum));
//                        final String all = total.getText().toString();
//                        totalamount = String.valueOf(sum);
//                        final String totalamount2 = bundle.getString("total_amount");
//                        if (counter >= 0){
//                            counter ++;
//                            items.setText(String.valueOf(counter));
//                            checkout.setVisibility(View.VISIBLE);
//                            items_ids.add(item_id);
//
//                            mProcessFav = true;
//
//
//                            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
//                            String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
//                            final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Businesses")
//                                    .child(uid).child(test).child("Tickets");
//                                databaseReference.addValueEventListener(new ValueEventListener() {
//                                    @Override
//                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                                        if (mProcessFav) {
//
//                                        if (dataSnapshot.hasChild(item_id))
//                                        {  DatabaseReference post = databaseReference.child(item_id);
//                                          String qu =  dataSnapshot.child(item_id).child("quantity").getValue().toString();
//                                          int qq = Integer.parseInt(qu);
//
//                                           post.child("quantity").setValue(String.valueOf(qq+1));
//                                            post.child("Product_Name").setValue(model.getProduct_Name());
//                                            post.child("Product_Price").setValue(model.getProduct_Price());
//                                            post.child("Product_Image").setValue(model.getProduct_Image());
//////                                            post.child("First_Name").setValue(Fname);
//                                            post.child("Total_Amount").setValue(all);
//                                            post.child("Last_Name").setValue(Lname);
//                                            post.child("Email").setValue(email);
//                                            post.child("Phone_Number").setValue(pnum);
//                                            post.child("Order_ID").setValue(a);
//                                            mProcessFav = false;
//                                        }
//                                        else {
//                                            DatabaseReference post = databaseReference.child(item_id);
//                                            post.child("Product_Name").setValue(model.getProduct_Name());
//                                            post.child("Product_Price").setValue(model.getProduct_Price());
//                                            post.child("Product_Image").setValue(model.getProduct_Image());
//                                            post.child("quantity").setValue("1");
////                                            post.child("Total_Amount").setValue(all);
////                                            post.child("First_Name").setValue(Fname);
////                                            post.child("Last_Name").setValue(Lname);
////                                            post.child("Email").setValue(email);
////                                            post.child("Phone_Number").setValue(pnum);
////                                            post.child("Order_ID").setValue(a);
//                                            mProcessFav = false;
//                                        }
//
//
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                                    }
//                                });
//
//
//                        }
//                        checkout.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
////                                    String tempIds = "";
//                                for (int x =0; x < items_ids.size();x++){
//
//                                    Intent intent = new Intent(getActivity(), Preview_items.class);
//                                    intent.putExtra("id",test);
//                                    intent.putExtra("first_name",Fname);
//                                    intent.putExtra("last_name",Lname);
//                                    intent.putExtra("user_key",a);
//                                    intent.putExtra("email", email);
//                                    intent.putExtra("pnum", pnum);
//                                    intent.putExtra("total_amount", totalamount2);
//
//                                    startActivity(intent);
//                                    Bungee.fade(getActivity());
//                                }
////                                    tempIds = tempIds.substring(1);
////                                    Toast.makeText(getActivity(), tempIds, Toast.LENGTH_SHORT).show();
//
//                            }
//                        });
//
//                    }
//                });
//            }
//        };
//
//
//        recyclerView.setAdapter(adapter);
//
//        list.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               Intent intent = new Intent(getActivity(), Preview_items.class);
////               intent.putExtra("array",items_ids);
//               intent.putExtra("id",test);
//          //     intent.putExtra("itemId",item_id);
////                intent.putExtra("total_amount",)
//               startActivity(intent);
//                Bungee.slideLeft(getActivity());
//            }
//        });
//
//        return view;
//    }

                holder.itemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        sum = sum+ Integer.parseInt(model.getProduct_Price());
                        total.setText(String.valueOf(sum));

                        final SharedPreferences preferences = getActivity().getSharedPreferences("total", Context.MODE_PRIVATE);
                        SharedPreferences.Editor  editor = preferences.edit();
                        editor.putInt("sum",sum);
                        editor.putString("itemId",item_id);
                        editor.apply();
                        if (counter >= 0){
                            counter ++;
                            items.setText(String.valueOf(counter));
                            checkout.setVisibility(View.VISIBLE);
                            items_ids.add(item_id);

                            mProcessFav = true;


                            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                            String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
                            final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Businesses")
                                    .child(uid).child(test).child("Tickets");
                            databaseReference.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (mProcessFav) {

                                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                                        String all = sharedPreferences.getString("sum", "");
//                                        if(!sum.equalsIgnoreCase("")){
//                                            final String all = sum;
//                                        }


                                        if (dataSnapshot.hasChild(item_id))
                                        {  DatabaseReference post = databaseReference.child(item_id);
                                            String qu =  dataSnapshot.child(item_id).child("quantity").getValue().toString();
                                            int qq = Integer.parseInt(qu);

                                            post.child("quantity").setValue(String.valueOf(qq+1));
                                            post.child("Product_Name").setValue(model.getProduct_Name());
                                            post.child("Product_Price").setValue(model.getProduct_Price());
                                            post.child("Product_Image").setValue(model.getProduct_Image());
                                            post.child("Total_Amount").setValue(all);
                                            post.child("Last_Name").setValue(Lname);
                                            post.child("Email").setValue(email);
                                            post.child("Phone_Number").setValue(pnum);
                                            post.child("Order_ID").setValue(a);
                                            mProcessFav = false;
                                        }
                                        else {
                                            DatabaseReference post = databaseReference.child(item_id);
                                            post.child("Product_Name").setValue(model.getProduct_Name());
                                            post.child("Product_Price").setValue(model.getProduct_Price());
                                            post.child("Product_Image").setValue(model.getProduct_Image());
                                            post.child("Total_Amount").setValue(all);
                                            post.child("Last_Name").setValue(Lname);
                                            post.child("Email").setValue(email);
                                            post.child("Phone_Number").setValue(pnum);
                                            post.child("Order_ID").setValue(a);
                                            post.child("quantity").setValue("1");
                                            mProcessFav = false;
                                        }


//                                            if (dataSnapshot.hasChild(item_id)) {
//                                                databaseReference.child(item_id).removeValue();
//                                                Toast.makeText(getActivity(), "Removed from  favourites"+item_id, Toast.LENGTH_SHORT).show();
//                                                mProcessFav = false;
//                                            } else {
//                                                databaseReference.child(item_id).setValue("Liked");
//                                                Toast.makeText(getActivity(), "Saved to  favourites"+item_id, Toast.LENGTH_SHORT).show();
//                                                mProcessFav = false;
//                                            }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });


                        }
                        checkout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                                    String tempIds = "";
//                                for (int x =0; x < items_ids.size();x++){
////                                        tempIds = tempIds + ", "+items_ids.get(x);
//                                    Toast.makeText(getActivity(), items_ids.get(x), Toast.LENGTH_SHORT).show();
//                                }
//                                    tempIds = tempIds.substring(1);
//                                    Toast.makeText(getActivity(), tempIds, Toast.LENGTH_SHORT).show();
//                                new FancyGifDialog.Builder(getActivity())
//                                        .setTitle("Customer Selection")
//                                        .setMessage("Please select the appropriate customer option")
//                                        .setNegativeBtnText("Walk-In")
//                                        .setPositiveBtnBackground("#FF4081")
//                                        .setPositiveBtnText("From Contacts")
//                                        .setNegativeBtnBackground("#FFA9A7A8")
//                                        .setGifResource(R.drawable.gif1)
//                                        .isCancellable(true)
//                                        .OnPositiveClicked(new FancyGifDialogListener() {
//                                            @Override
//                                            public void OnClick() {
//                                                dialogBasicTags fragment = new dialogBasicTags();
////                                                //pass id to frament(dialog)
////                                                Bundle bundle = new Bundle();
////                                                bundle.putString("id", test);
////
////                                                fragment.show(getActivity().getSupportFragmentManager(), fragment.getTag());
////                                                fragment.setArguments(bundle);
//
//                                                Toast.makeText(getContext(),"Update coming shortly",Toast.LENGTH_SHORT);
//                                            }
//                                        })
//                                        .OnNegativeClicked(new FancyGifDialogListener() {
//                                            @Override
//                                            public void OnClick() {
//                                                Fragment fragment = new Home();
//                                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                                fragmentTransaction.replace(R.id.frame, fragment);
//                                                fragmentTransaction.addToBackStack(null);
//                                                fragmentTransaction.commit();
//                                            }
//                                        })
//                                        .build();

                                Intent intent = new Intent(getActivity(), Preview_items.class);
                                intent.putExtra("id",test);
                                intent.putExtra("first_name",Fname);
                                intent.putExtra("last_name",Lname);
                                intent.putExtra("user_key",a);
                                intent.putExtra("email", email);
                                intent.putExtra("pnum", pnum);


                                    startActivity(intent);
                                    Bungee.fade(getActivity());

                            }
                        });

                    }
                });
            }
        };


        recyclerView.setAdapter(adapter);

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Preview_items.class);
//               intent.putExtra("array",items_ids);
                intent.putExtra("id",test);
                //     intent.putExtra("itemId",item_id);
                startActivity(intent);
                Bungee.slideLeft(getActivity());
            }
        });

        return view;
    }



    private class itemsViewHolder extends RecyclerView.ViewHolder {
        View mView;

        private TextView stkk,ppname,pprc,iimg;

        public ImageView imageView;


        public itemsViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            stkk = mView.findViewById(R.id.stock);
            ppname = mView.findViewById(R.id.p_name);
            pprc = mView.findViewById(R.id.price);
            imageView = mView.findViewById(R.id.productimage);

        }


        public void setStk(String stk){
            stkk.setText(stk);
        }

        public void setPpname(String pname) {
            ppname.setText(pname);
        }


        public void setPprc(String prc) {
            pprc.setText(prc);
        }

        public void setImageView(String image)
        {
            Picasso.get().load(image).into(imageView);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sales, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sales_shop :
                Toast.makeText(getActivity(), "Switch Layout", Toast.LENGTH_SHORT).show();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null) {
            adapter.stopListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }
    }
}