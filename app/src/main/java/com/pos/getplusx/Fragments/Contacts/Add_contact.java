package com.pos.getplusx.Fragments.Contacts;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.pos.getplusx.R;

public class Add_contact extends Fragment {

    EditText fname,lname,add1,add2,cty,state,zip,country,pnumber;
    TextView save;


    private FirebaseAuth auth;
    private FirebaseUser mCurrentUser;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    public Add_contact() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_contact, container, false);

        fname = view.findViewById(R.id.name3);
        lname = view.findViewById(R.id.name4);
        add1 = view.findViewById(R.id.addL2);
        add2 = view.findViewById(R.id.addL4);
        cty = view.findViewById(R.id.city2);
        state = view.findViewById(R.id.state2);
        zip = view.findViewById(R.id.zip2);
        country = view.findViewById(R.id.country2);
        pnumber = view.findViewById(R.id.phn);
        save = view.findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadcontact();
            }
        });

        return  view;
    }

    private void uploadcontact() {


        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = firebaseAuth.getCurrentUser().getUid();

        final String f =   fname.getText().toString();
        final String l = lname.getText().toString();
        final String a1 = add1.getText().toString();
        final String a2 = add2.getText().toString();
        final String c = cty.getText().toString();
        final String s = state.getText().toString();
        final String z = zip.getText().toString();
        final String cc = country.getText().toString();
        final String p = pnumber.getText().toString();


        if (!TextUtils.isEmpty(f) && !TextUtils.isEmpty(l)&& !TextUtils.isEmpty(a1)&& !TextUtils.isEmpty(a2)&& !TextUtils.isEmpty(c) && !TextUtils.isEmpty(s)  && !TextUtils.isEmpty(z) && !TextUtils.isEmpty(cc)&& !TextUtils.isEmpty(z) && !TextUtils.isEmpty(cc)){



            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Businesses").child(uid).child("Contacts").push();
            databaseReference.child("First_Name").setValue(f);
            databaseReference.child("Last_Name").setValue(l);
            databaseReference.child("Address_One").setValue(a1);
            databaseReference.child("Address_Two").setValue(a2);
            databaseReference.child("City").setValue(c);
            databaseReference.child("State").setValue(s);
            databaseReference.child("Zip").setValue(z);
            databaseReference.child("Country").setValue(cc);
            databaseReference.child("Phone_Number").setValue(p);
            databaseReference.child("Time_Stamp").setValue(ServerValue.TIMESTAMP)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            Toast.makeText(getActivity(), "Successfully Uploaded",Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){

                                Fragment fragment = new Contacts();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();

                            }

                            else

                                Toast.makeText(getActivity(), "An error occurred, Please Retry Again",Toast.LENGTH_SHORT).show();


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(getActivity(), "An error occurred, Please Retry Again",Toast.LENGTH_SHORT).show();

                }
            });


//            ProgressDialog pDialog = new ProgressDialog(getContext());
//            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            pDialog.setTitle("Please Wait...");
//            pDialog.setMessage("Uploading Internship Data");
//            pDialog.setCancelable(false);
//            pDialog.show();

        }

        else {

            //LGSnackbarManager.show(LGSnackBarTheme.SnackbarStyle.ERROR, "Please fill all fields!");
            Toast.makeText(getActivity(), "Please Check and and Retry Again",Toast.LENGTH_SHORT).show();
        }
    }


}
