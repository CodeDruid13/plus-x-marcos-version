//package com.pos.getplusx.Fragments.Contacts;
//
//import android.content.Context;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.DividerItemDecoration;
//import android.support.v7.widget.GridLayoutManager;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.firebase.ui.database.FirebaseRecyclerAdapter;
//import com.firebase.ui.database.FirebaseRecyclerOptions;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.Query;
//import com.pos.getplusx.R;
//import com.pos.getplusx.model.ContactsViewHolder;
//import com.pos.getplusx.model.contactsmodel;
//import com.squareup.picasso.Picasso;
//
//import java.util.Objects;
//
//import static android.widget.LinearLayout.VERTICAL;
//
//
//public class Contacts extends Fragment {
//
//
//    RecyclerView mRecycler;
//    Context context;
//    FirebaseRecyclerAdapter adapter;
//    DatabaseReference myref;
//
//
//
//    public Contacts() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View rootview = inflater.inflate(R.layout.fragment_contacts, container, false);
//
//        context = getActivity();
//        mRecycler = rootview.findViewById(R.id.contactsrecylerview);
//
//        if (mRecycler != null) {
//            //to enable optimization of recyclerview
//            mRecycler.setHasFixedSize(true);
//        }
//
//        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
//        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
//
//        myref = FirebaseDatabase.getInstance().getReference().child("Businesses").child(uid).child("Products");
//        myref.keepSynced(true);
//
//        RecyclerView recyclerView = rootview.findViewById(R.id.items);
//        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
////        recyclerView.setItemAnimator(new DefaultItemAnimator());
////        DividerItemDecoration decoration = new DividerItemDecoration(getActivity(), VERTICAL);
////        recyclerView.addItemDecoration(decoration);
//
//
//        FirebaseRecyclerOptions<contactsmodel> options =
//                new FirebaseRecyclerOptions.Builder<contactsmodel>()
//                        .setQuery(myref, contactsmodel.class)
//                        .build();
//        adapter = new FirebaseRecyclerAdapter<contactsmodel, ContactsViewHolder>(options) {
//            @NonNull
//            @Override
//            public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//
//                View s = LayoutInflater.from(getActivity())
//                        .inflate(R.layout.contacts, parent, false);
//
//                return new ContactsViewHolder(s);
//            }
//
//            @Override
//            protected void onBindViewHolder(@NonNull ContactsViewHolder holder, int position, @NonNull contactsmodel model) {
//                final String item_id = getRef(position).getKey();
//
//
//            }
//        };
//
//
//        recyclerView.setAdapter(adapter);
//
//
//        return rootview;
//    }
//
//
//
//}

package com.pos.getplusx.Fragments.Contacts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.pos.getplusx.Fragments.Sales.Sales;
import com.pos.getplusx.R;

import java.util.Objects;

import static android.widget.LinearLayout.VERTICAL;


public class Contacts extends Fragment {

    private GridLayoutManager mLayoutManager;
    private FirebaseRecyclerAdapter mTextRecyclerAdapter;
    RecyclerView mRecycler;
    Context context;
    String Log = "DB";

    // Firebase Auth Object.
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myref;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private Query mTextQuery;
    String test;

    public Contacts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_contacts, container, false);

        context = getActivity();
        mRecycler = rootview.findViewById(R.id.contactsrecylerview);
        Bundle bundle = this.getArguments();
        assert bundle != null;
        test = bundle.getString("id");

        if (mRecycler != null) {
            //to enable optimization of recyclerview
            mRecycler.setHasFixedSize(true);
        }

        //get current user
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        myref = FirebaseDatabase.getInstance().getReference().child("Businesses").child(uid).child(test).child("Contacts");
        myref.keepSynced(true);


        return rootview;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        //Set up Layout Manager, reverse layout
//        mLayoutManager = new GridLayoutManager(getActivity(), 2);
//        mLayoutManager.setReverseLayout(false);
//        //mLayoutManager.setStackFromEnd(true);
//        mRecycler.setLayoutManager(mLayoutManager);

        mRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration decoration = new DividerItemDecoration(getActivity(), VERTICAL);
        mRecycler.addItemDecoration(decoration);


        // Set up FirebaseRecyclerAdapter with the Query
        Query postsQuery = myref;

        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<ContactsModel>()
                .setQuery(postsQuery, ContactsModel.class)
                .build();

        mTextRecyclerAdapter =  new FirebaseRecyclerAdapter<ContactsModel, ContactsViewHolder>(options){

            @NonNull
            @Override
            public ContactsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                return new ContactsViewHolder(inflater.inflate(R.layout.contacts, viewGroup, false));
            }

            @Override
            protected void onBindViewHolder(@NonNull ContactsViewHolder holder, int position, final ContactsModel model) {

                final String user_key = getRef(position).getKey();

                //letter = String.valueOf(dataList.get(i).charAt(0));
//                letter = String.valueOf()

                holder.setFname(model.getFirst_Name());
                holder.setLname(model.getLast_Name());
                holder.setPnum(model.getPhone_Number());
                holder.setEmail(model.getEmail());



                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Bundle bundle = new Bundle();
                        Intent intent = getActivity().getIntent();
                        String id = intent.getStringExtra("id");
                        bundle.putString("id", id);
                        bundle.putString("user_key", user_key);
                        bundle.putString("first_name", model.getFirst_Name());
                        bundle.putString("last_name", model.getLast_Name());
                        bundle.putString("phone_number", model.getPhone_Number());
                        bundle.putString("email", model.getEmail());
                        Fragment fragment = new Sales();
                        fragment.setArguments(bundle);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
                });

            }
        };

        mRecycler.setAdapter(mTextRecyclerAdapter);


    }


    @Override
    public void onStart() {
        super.onStart();

        if (mTextRecyclerAdapter != null  ){

            mTextRecyclerAdapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mTextRecyclerAdapter != null  ){

            mTextRecyclerAdapter.startListening();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mTextRecyclerAdapter != null  ){

            mTextRecyclerAdapter.startListening();
        }
    }


    @Override
    public void onStop() {
        super.onStop();

        if (mTextRecyclerAdapter != null  ){

            mTextRecyclerAdapter.stopListening();
        }
    }



}