//package com.pos.getplusx.Fragments;
//
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.FrameLayout;
//import android.widget.LinearLayout;
//import android.widget.Toast;
//
//import com.pos.getplusx.R;
//import com.pos.getplusx.Activities.Shops.ShopsActivity;
//import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
//import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;
//
//import spencerstudios.com.bungeelib.Bungee;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class Home extends Fragment {
//
//
//
//    public Home() {
//        // Required empty public constructor
//    }
//View view;
//    FrameLayout shop, sales, Contacts, products,frameLayout;
//    LinearLayout frame;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        view =  inflater.inflate(R.layout.fragment_home, container, false);
//
//        shop = view.findViewById(R.id.shop);
//        sales = view.findViewById(R.id.sales);
//        Contacts = view.findViewById(R.id.addcontacts);
//        products = view.findViewById(R.id.productsio);
//        shop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), ShopsActivity.class));
//                Bungee.slideRight(getActivity());
//            }
//        });
//        sales.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                replace_fragment(new Sales());
//                Bungee.slideRight(getActivity());
//            }
//        });
//        Contacts.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new FancyGifDialog.Builder(getActivity())
//                        .setTitle("Customer Selection")
//                        .setMessage("Please select the appropriate customer option")
//                        .setNegativeBtnText("Walk-In")
//                        .setPositiveBtnBackground("#FF4081")
//                        .setPositiveBtnText("From Contacts")
//                        .setNegativeBtnBackground("#FFA9A7A8")
//                        .setGifResource(R.drawable.gif1)
//                        .isCancellable(true)
//                        .OnPositiveClicked(new FancyGifDialogListener() {
//                            @Override
//                            public void OnClick() {
////                                Fragment fragment = new Contacts();
////                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
////                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
////                                fragmentTransaction.replace(R.id.frame, fragment);
////                                fragmentTransaction.addToBackStack(null);
////                                fragmentTransaction.commit();
//
//                                Toast.makeText(getContext(),"Update coming shortly",Toast.LENGTH_SHORT);
//                            }
//                        })
//                        .OnNegativeClicked(new FancyGifDialogListener() {
//                            @Override
//                            public void OnClick() {
//                                Fragment fragment = new Add_contact();
//                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.frame, fragment);
//                                fragmentTransaction.addToBackStack(null);
//                                fragmentTransaction.commit();
//                            }
//                        })
//                        .build();
//            }
//        });
//        products.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = getIntent();
//                String id = intent.getStringExtra("id");
//                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                Bundle bundle = new Bundle();
//                bundle.putString("id", id);
//                Products frag = new Products();
//                frag.setArguments(bundle);
//                transaction.replace(R.id.frame, frag);
//                transaction.commit();
//                toolbar.setTitle("Products");
//            }
//        });
//
//
//
//
//        return view;
//    }
//    public void replace_fragment(Fragment fragment) {
//        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame, fragment);
//        transaction.commit();
//    }
//
//
//
//}


package com.pos.getplusx.Fragments.Home;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pos.getplusx.Activities.Shops.ShopsActivity;
import com.pos.getplusx.Fragments.Contacts.Add_contact;
import com.pos.getplusx.Fragments.Contacts.Contacts;
import com.pos.getplusx.Fragments.Products.Products;
import com.pos.getplusx.Fragments.Sales.Sales;
import com.pos.getplusx.R;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

import spencerstudios.com.bungeelib.Bungee;

/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment {



    public Home() {
        // Required empty public constructor
    }
    View view;
    FrameLayout shop, sales, contacts, products,frameLayout;
    LinearLayout frame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_home, container, false);


        shop = view.findViewById(R.id.shop);
        sales = view.findViewById(R.id.sales);
        contacts = view.findViewById(R.id.addcontacts);
        products = view.findViewById(R.id.productsio);
        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ShopsActivity.class));
                Bungee.slideRight(getActivity());
            }
        });

        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                new FancyGifDialog.Builder(getActivity())
                        .setTitle("Customer Selection")
                        .setMessage("Please select the appropriate customer option")
                        .setNegativeBtnText("Walk-In")
                        .setPositiveBtnBackground("#FF4081")
                        .setPositiveBtnText("From Contacts")
                        .setNegativeBtnBackground("#FFA9A7A8")
                        .setGifResource(R.drawable.gif1)
                        .isCancellable(true)
                        .OnPositiveClicked(new FancyGifDialogListener() {
                            @Override
                            public void OnClick() {

                                Bundle bundle = new Bundle();
                                Intent intent = getActivity().getIntent();
                                String id = intent.getStringExtra("id");
                                bundle.putString("id", id);
                                Fragment fragment = new Contacts();
                                fragment.setArguments(bundle);
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();


                            }
                        })
                        .OnNegativeClicked(new FancyGifDialogListener() {
                            @Override
                            public void OnClick() {

                                Fragment fragment = new Add_contact();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }
                        })
                        .build();

            }
        });
        contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Bundle bundle = new Bundle();
                Intent intent = getActivity().getIntent();
                String id = intent.getStringExtra("id");
                bundle.putString("id", id);
                Fragment fragment = new Contacts();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();


            }



        });

        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                Intent intent = getActivity().getIntent();
                String id = intent.getStringExtra("id");
                bundle.putString("id", id);
                Fragment fragment = new Products();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });



        return view;
    }
    public void replace_fragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();
    }



}