package com.pos.getplusx.Fragments.Products;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.pos.getplusx.R;
import com.pos.getplusx.model.ProductsViewHolder;
import com.pos.getplusx.model.items;

import java.util.Objects;


public class Products extends Fragment {

    private GridLayoutManager mLayoutManager;
    private FirebaseRecyclerAdapter mTextRecyclerAdapter;
    RecyclerView mRecycler;
    Context context;
    String Log = "DB";

    // Firebase Auth Object.
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myref;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private Query mTextQuery;
     String test;

    FloatingActionButton floatingActionButton;

    public Products() {
        // Required empty public constructor
    }


    //@SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_products, container, false);

        context = getActivity();
        mRecycler = rootview.findViewById(R.id.allproducts);
        Bundle bundle = this.getArguments();
        assert bundle != null;
        test = bundle.getString("id");

        if (mRecycler != null) {
            //to enable optimization of recyclerview
            mRecycler.setHasFixedSize(true);
        }

        //get current user
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        myref = FirebaseDatabase.getInstance().getReference().child("Businesses").child(uid).child(test).child("Products");
        myref.keepSynced(true);
        return  rootview;
    }


        @Override
        public void onActivityCreated (@Nullable Bundle savedInstanceState){
            super.onActivityCreated(savedInstanceState);

            //Set up Layout Manager, reverse layout
            mLayoutManager = new GridLayoutManager(getActivity(), 2);
            mLayoutManager.setReverseLayout(false);
            //mLayoutManager.setStackFromEnd(true);
            mRecycler.setLayoutManager(mLayoutManager);

            FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<items>()
                    .setQuery(myref, items.class)
                    .build();

            mTextRecyclerAdapter = new FirebaseRecyclerAdapter<items, ProductsViewHolder>(options) {

                @NonNull
                @Override
                public ProductsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
                    LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                    return new ProductsViewHolder(inflater.inflate(R.layout.products_recyclerview, viewGroup, false));
                }

                @Override
                protected void onBindViewHolder(@NonNull ProductsViewHolder holder, int position, final items model) {

                    final String user_key = getRef(position).getKey();

                    holder.setStk(model.getStock_Quantity());
                    holder.setPpname(model.getProduct_Name());
                    holder.setPprc(model.getProduct_Price());
                    holder.setImageView(model.getProduct_Image());

                }
            };

            mRecycler.setAdapter(mTextRecyclerAdapter);


        }



        @Override
        public void onStart () {
            super.onStart();

            if (mTextRecyclerAdapter != null) {

                mTextRecyclerAdapter.startListening();
            }
        }

        @Override
        public void onResume () {
            super.onResume();

            if (mTextRecyclerAdapter != null) {

                mTextRecyclerAdapter.startListening();
            }
        }

        @Override
        public void onPause () {
            super.onPause();

            if (mTextRecyclerAdapter != null) {

                mTextRecyclerAdapter.startListening();
            }
        }


        @Override
        public void onStop () {
            super.onStop();

            if (mTextRecyclerAdapter != null) {

                mTextRecyclerAdapter.stopListening();
            }
        }


    }
