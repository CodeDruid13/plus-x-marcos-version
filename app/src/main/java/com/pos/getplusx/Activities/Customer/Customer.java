package com.pos.getplusx.Activities.Customer;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.pos.getplusx.R;

public class Customer extends AppCompatActivity {

    EditText phone1,phone2,phone_work,fname,lname,mname,email,job_title,dept,com_name,p_address,m_address,website,fb,linked_in;
    ProgressBar progressBar;
    Button save,new_cust,contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        new_cust = findViewById(R.id.new_customer);
        new_cust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });
    }

    private void showCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.new_customer_dialog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        //get views from dialog
//        name = dialog.findViewById(R.id.first_name);
//        lname = dialog.findViewById(R.id.l_name);
//        mname = dialog.findViewById(R.id.m_name);
//        phone1 = dialog.findViewById(R.id.phone1);
//        phone2 = dialog.findViewById(R.id.phone2);
//        phone_work = dialog.findViewById(R.id.phone_work);
//        email = dialog.findViewById(R.id.email_contact);
//        job_title = dialog.findViewById(R.id.contacts_job);
//        dept = dialog.findViewById(R.id.contacts_dept);
//        com_name = dialog.findViewById(R.id.company_name);
//        p_address = dialog.findViewById(R.id.contacts_address);
//        m_address = dialog.findViewById(R.id.contacts_mail_address);
//        website = dialog.findViewById(R.id.contacts_website);
//        fb = dialog.findViewById(R.id.facebook);
//        linked_in = dialog.findViewById(R.id.linked_in);
//        progressBar = dialog.findViewById(R.id.loading_contact);
//        save = dialog.findViewById(R.id.bt_save);




        ( dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



        ( dialog.findViewById(R.id.bt_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                createContact();

                //  dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
