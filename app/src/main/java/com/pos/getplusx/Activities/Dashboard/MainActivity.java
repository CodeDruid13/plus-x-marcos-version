//package com.pos.getplusx.Activities;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v7.widget.CardView;
//import android.view.Gravity;
//import android.support.v4.view.GravityCompat;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.view.MenuItem;
//import android.support.design.widget.NavigationView;
//import android.support.v4.widget.DrawerLayout;
//
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.Menu;
//import android.view.View;
//import android.widget.ImageButton;
//import android.widget.Toast;
//
//import com.pos.getplusx.R;
//import com.pos.getplusx.Fragments.Home.Home;
//import com.pos.getplusx.Fragments.Products.Products;
//import com.pos.getplusx.Fragments.Sales.Sales;
//import com.pos.getplusx.Fragments.Settings.Settings;
//import com.rupins.drawercardbehaviour.CardDrawerLayout;
//
//public class MainActivity extends AppCompatActivity
//        implements /*NavigationView.OnNavigationItemSelectedListener,*/ View.OnClickListener {
//    private CardDrawerLayout drawer;
//    NavigationView navigationView;
//    Toolbar toolbar;
//
//    //custom side menu
//    CardView one,two,three,four,five,six;
//    ImageButton imageButton,imageButton2,imageButton3,imageButton4,imageButton5,imageButton6;
//
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//
//
//        imageButton = findViewById(R.id.goto_home);
//        imageButton.setBackgroundColor(getResources().getColor(R.color.Home));
//        imageButton2 =  findViewById(R.id.goto_chats);
//        imageButton2.setBackgroundColor(getResources().getColor(R.color.Sales));
//        imageButton3 = findViewById(R.id.goto_internships);
//        imageButton4 = findViewById(R.id.goto_profile);
//        imageButton4.setBackgroundColor(getResources().getColor(R.color.Products));
//        imageButton5 = findViewById(R.id.goto_help);
//        imageButton5.setBackgroundColor(getResources().getColor(R.color.back_office));
//        imageButton6 = findViewById(R.id.goto_about);
//        imageButton6.setBackgroundColor(getResources().getColor(R.color.Settings));
//
//        imageButton.setOnClickListener(this);
//        imageButton2.setOnClickListener(this);
//        imageButton3.setOnClickListener(this);
//        imageButton4.setOnClickListener(this);
//        imageButton5.setOnClickListener(this);
//        imageButton6.setOnClickListener(this);
//
//
//        drawer = findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//        drawer.setViewScale(Gravity.START, 0.9f);
//        drawer.setRadius(Gravity.START, 35);
//        drawer.setViewElevation(Gravity.START, 20);
//
////        navigationView = findViewById(R.id.nav_view);
////        navigationView.setNavigationItemSelectedListener(this);
////        navigationView.setItemIconTintList(null);
////        navigationView.setCheckedItem(R.id.nav_home);
//        replace_fragment(new Home());
//        toolbar.setTitle("getPlusx");
//    }
//
//
//
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    public void replace_fragment(Fragment fragment) {
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame, fragment);
//        transaction.commit();
//    }
//
//    @Override
//    public void onClick(View view) {
//    if (view ==imageButton){
//                drawer.closeDrawer(GravityCompat.START);
//                replace_fragment(new Home());
//                toolbar.setTitle("Shops");
//    }
//
//        if (view ==imageButton2){
//            drawer.closeDrawer(GravityCompat.START);
//            Intent intent = getIntent();
//            String id = intent.getStringExtra("id");
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            Bundle bundle = new Bundle();
//            bundle.putString("id", id);
//            Sales frag = new Sales();
//            frag.setArguments(bundle);
//            transaction.replace(R.id.frame, frag);
//            transaction.commit();;
//            toolbar.setTitle("Sales");
//        }
//
//        if (view ==imageButton3){
//            drawer.closeDrawer(GravityCompat.START);
//            toolbar.setTitle("Receipts");
//        }
//        if (view ==imageButton4){
//            drawer.closeDrawer(GravityCompat.START);
//
//            Intent intent = getIntent();
//            String id = intent.getStringExtra("id");
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            Bundle bundle = new Bundle();
//            bundle.putString("id", id);
//            Products frag = new Products();
//            frag.setArguments(bundle);
//            transaction.replace(R.id.frame, frag);
//            transaction.commit();
//            toolbar.setTitle("Products");
//        }
//        if (view ==imageButton5){
//            drawer.closeDrawer(GravityCompat.START);
//            replace_fragment(new Home());
//            toolbar.setTitle("Shops");
//        }
//        if (view ==imageButton6){
//            drawer.closeDrawer(GravityCompat.START);
//            replace_fragment(new Settings());
//            toolbar.setTitle("Settings");
//        }
//    }
//}

package com.pos.getplusx.Activities.Dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.pos.getplusx.Fragments.Contacts.Add_contact;
import com.pos.getplusx.Fragments.Contacts.Contacts;
import com.pos.getplusx.Fragments.Home.Home;
import com.pos.getplusx.Fragments.Products.Products;
import com.pos.getplusx.Fragments.Settings.Settings;
import com.pos.getplusx.R;
import com.pos.getplusx.Fragments.Receipts.Receipts;
import com.rupins.drawercardbehaviour.CardDrawerLayout;
import com.shashank.sony.fancygifdialoglib.FancyGifDialog;
import com.shashank.sony.fancygifdialoglib.FancyGifDialogListener;

public class MainActivity extends AppCompatActivity
        implements /*NavigationView.OnNavigationItemSelectedListener,*/ View.OnClickListener {
    private CardDrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar;

    //custom side menu
    CardView one,two,three,four,five,six;
    ImageButton imageButton,imageButton2,imageButton3,imageButton4,imageButton5,imageButton6;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        imageButton = findViewById(R.id.goto_home);
        imageButton.setBackgroundColor(getResources().getColor(R.color.home));
        imageButton2 =  findViewById(R.id.goto_chats);
        imageButton2.setBackgroundColor(getResources().getColor(R.color.sales));
        imageButton3 = findViewById(R.id.goto_internships);
        imageButton3.setBackgroundColor(getResources().getColor(R.color.receipts));
        imageButton4 = findViewById(R.id.goto_profile);
        imageButton4.setBackgroundColor(getResources().getColor(R.color.products));
        imageButton5 = findViewById(R.id.goto_help);
        imageButton5.setBackgroundColor(getResources().getColor(R.color.back_office));
        imageButton6 = findViewById(R.id.goto_about);
        imageButton6.setBackgroundColor(getResources().getColor(R.color.settings));

        imageButton.setOnClickListener(this);
        imageButton2.setOnClickListener(this);
        imageButton3.setOnClickListener(this);
        imageButton4.setOnClickListener(this);
        imageButton5.setOnClickListener(this);
        imageButton6.setOnClickListener(this);


        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setRadius(Gravity.START, 35);
        drawer.setViewElevation(Gravity.START, 20);

//        navigationView = findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//        navigationView.setItemIconTintList(null);
//        navigationView.setCheckedItem(R.id.nav_home);
        replace_fragment(new Home());
        toolbar.setTitle("getPlusx");
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds Items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void replace_fragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();
    }

    @Override
    public void onClick(View view) {
        if (view ==imageButton){
            drawer.closeDrawer(GravityCompat.START);
            replace_fragment(new Home());
            toolbar.setTitle("Shops");
        }

        if (view ==imageButton2){
            drawer.closeDrawer(GravityCompat.START);

//            replace_fragment(new Sales());
//            toolbar.setTitle("Sales");
//            Intent intent = getIntent();
//            String id = intent.getStringExtra("id");
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            Bundle bundle = new Bundle();
//            bundle.putString("id", id);
//            Sales frag = new Sales();
//            frag.setArguments(bundle);
//            transaction.replace(R.id.frame, frag);
//            transaction.commit();
//            toolbar.setTitle("Sales");

            new FancyGifDialog.Builder(this)
                    .setTitle("Customer Selection")
                    .setMessage("Please select the appropriate customer option")
                    .setNegativeBtnText("Walk-In")
                    .setPositiveBtnBackground("#FF4081")
                    .setPositiveBtnText("From Contacts")
                    .setNegativeBtnBackground("#FFA9A7A8")
                    .setGifResource(R.drawable.gif1)
                    .isCancellable(true)
                    .OnPositiveClicked(new FancyGifDialogListener() {
                        @Override
                        public void OnClick() {

                            Bundle bundle = new Bundle();
                            Intent intent = getIntent();
                            String id = intent.getStringExtra("id");
                            bundle.putString("id", id);
                            Fragment fragment = new Contacts();
                            fragment.setArguments(bundle);
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.frame, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                            Toast.makeText(MainActivity.this,"Update coming shortly",Toast.LENGTH_SHORT);
                        }
                    })
                    .OnNegativeClicked(new FancyGifDialogListener() {
                        @Override
                        public void OnClick() {
                            Fragment fragment = new Add_contact();
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.frame, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();
                        }
                    })
                    .build();
        }

        if (view ==imageButton3){
            drawer.closeDrawer(GravityCompat.START);
//
//            replace_fragment(new Receipts());
//            toolbar.setTitle("Receipts");
            Intent intent = getIntent();
            String id = intent.getStringExtra("id");
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("id", id);
            Receipts frag = new Receipts();
            frag.setArguments(bundle);
            transaction.replace(R.id.frame, frag);
            transaction.commit();
            toolbar.setTitle("Receipts");
        }
        if (view ==imageButton4){
            drawer.closeDrawer(GravityCompat.START);

            Intent intent = getIntent();
            String id = intent.getStringExtra("id");
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("id", id);
            Products frag = new Products();
            frag.setArguments(bundle);
            transaction.replace(R.id.frame, frag);
            transaction.commit();
            toolbar.setTitle("Products");
        }
        if (view ==imageButton5){
            drawer.closeDrawer(GravityCompat.START);

            replace_fragment(new Home());
            toolbar.setTitle("Shops");

        }
        if (view ==imageButton6){
            drawer.closeDrawer(GravityCompat.START);
            replace_fragment(new Settings());
            toolbar.setTitle("Settings");
        }
    }
}