package com.pos.getplusx.Activities.Preview_Checkout;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pos.getplusx.R;
import com.pos.getplusx.model.items;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import static android.widget.LinearLayout.VERTICAL;

public class Preview_items extends AppCompatActivity {

    FirebaseRecyclerAdapter adapter;
    DatabaseReference myref;
    TextView items,checkout,total;
    Button proceed;

    ImageView clear;

    String test,one,two,three,four,five;
    String item_total;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_items);


        test = getIntent().getStringExtra("id");
        one = getIntent().getStringExtra("user_key");
        two = getIntent().getStringExtra("first_name");
        three = getIntent().getStringExtra("last_name");
        four = getIntent().getStringExtra("email");
        five = getIntent().getStringExtra("phone_number");
        item_total = getIntent().getStringExtra("total_amount");
//        item_total = Integer.parseInt(getIntent().getExtras().get("total_amount").toString());



//        SharedPreferences preferences = getSharedPreferences("total",MODE_PRIVATE);
//        final int sum = preferences.getInt("sum",0);
//        String itemId = preferences.getString("itemId","default");
//
//        String contactId = getIntent().getStringExtra("contact_id");
//        String test = getIntent().getStringExtra("test");
//
//        Toast.makeText(this,itemId+test , Toast.LENGTH_SHORT).show();

        proceed =findViewById(R.id.takitaki);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Preview_items.this, item_total, Toast.LENGTH_SHORT).show();
                //dialogBasicTags = new dialogBasicTags();
            }
        });


        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        myref = FirebaseDatabase.getInstance().getReference().child("Businesses")
                .child(uid).child(test).child("Tickets");


        myref.keepSynced(true);




        RecyclerView recyclerView = findViewById(R.id.preview_recyclerview);
//        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration decoration = new DividerItemDecoration(this, VERTICAL);
        recyclerView.addItemDecoration(decoration);


            FirebaseRecyclerOptions<com.pos.getplusx.model.items> options =
                    new FirebaseRecyclerOptions.Builder<items>()
                            .setQuery(myref, items.class)
                            .build();


            adapter = new FirebaseRecyclerAdapter<items, itemsViewHolder>(options) {

                @NonNull
                @Override
                public itemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                    View s = LayoutInflater.from(Preview_items.this)
                            .inflate(R.layout.item_product_preview, parent, false);

                    return new itemsViewHolder(s);
                }

                @Override
                protected void onBindViewHolder(@NonNull itemsViewHolder holder, final int position, @NonNull final items model) {
                    final String Pre_item_id = getRef(position).getKey();

                    //holder.setStk(model.getStock_Quantity());
                    holder.setPpname(model.getProduct_Name());
                    holder.setPprc(model.getProduct_Price());
                    holder.setImageView(model.getProduct_Image());
                    holder.quanti.setText(model.getQuantity());

                    String units = model.getQuantity();
                    final String price =model.getProduct_Price();
                    Long unitss = Long.parseLong(units);
                    final Long pricee = Long.parseLong(price);

                    final Long total= unitss*pricee;

                    holder.itemView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                            String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
                            DatabaseReference databaseReference = myref.child(Pre_item_id);
                           // Toast.makeText(Preview_items.this, pricee,Toast.LENGTH_SHORT).show();

                        }
                    });

                    holder.close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            myref.child(Pre_item_id).setValue(null);


                        }
                    });
                }
            };


            recyclerView.setAdapter(adapter);


    }

    private class itemsViewHolder extends RecyclerView.ViewHolder {
        View mView;

        private TextView stkk,ppname,pprc,iimg,quanti;

        public ImageView imageView,close;


        public itemsViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            //stkk = mView.findViewById(R.id.stock);
            ppname = mView.findViewById(R.id.p_name);
            pprc = mView.findViewById(R.id.price);
            imageView = mView.findViewById(R.id.productimage);
            quanti = mView.findViewById(R.id.quant);
            close = mView.findViewById(R.id.remove);

        }


//        public void setStk(String stk){
//            stkk.setText(stk);
//        }

        public void setPpname(String pname) {
            ppname.setText(pname);
        }


        public void setPprc(String prc) {
            pprc.setText(prc);
        }

        public void setImageView(String image)
        {
            Picasso.get().load(image).into(imageView);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null) {
            adapter.stopListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null) {
            adapter.stopListening();
        }
    }
}