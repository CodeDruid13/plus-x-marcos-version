package com.pos.getplusx.Activities.Preview_Checkout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.pos.getplusx.R;
import com.pos.getplusx.model.contactsmodel;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.widget.LinearLayout.VERTICAL;

public class dialogBasicTags extends DialogFragment {


    DatabaseReference myref,myRef;
    FirebaseRecyclerAdapter adapter;
    String query_text;
    Context context;
    private StorageReference storageReference;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_contacts, container, false);
        getDialog().setTitle("Simple Dialog");

            final String test = getArguments().getString("id");
            context = getActivity();


        RecyclerView recyclerView =  rootView.findViewById(R.id.recyclerView_di);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration decoration = new DividerItemDecoration(getActivity(), VERTICAL);

        //get current user
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        recyclerView.addItemDecoration(decoration);

        myref = FirebaseDatabase.getInstance().getReference().child("Businesses").child(uid).child(test).child("Contacts");
        myref.keepSynced(true);
        FirebaseRecyclerOptions<contactsmodel> options =  new FirebaseRecyclerOptions.Builder<contactsmodel>()
                .setQuery(myref, contactsmodel.class)
                .build();


        adapter = new FirebaseRecyclerAdapter<contactsmodel, ContactsViewHolder>(options) {
            @NonNull
            @Override
            public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View s = LayoutInflater.from(getActivity())
                        .inflate(R.layout.item_inbox, parent, false);

                return new ContactsViewHolder(s);
            }


            @Override
            protected void onBindViewHolder(@NonNull final ContactsViewHolder holder, int position, @NonNull final contactsmodel model) {
                final String contact_id = getRef(position).getKey();
                // displaying text view data
                holder.name.setText(model.getFirst_Name());
                holder.email.setText(model.getCity());
                holder.phone.setText(model.getPhone_Number());
                holder.image_letter.setText(model.getFirst_Name().substring(0,1));
                holder.image_letter.setVisibility(View.VISIBLE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        EditDialogListener activity = (EditDialogListener) getActivity();
//                        activity.updateResult(image, model.getFirst_name(),contact_id);

                        new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme)
                                .setTitle("Proceed to checkout??")
                                .setMessage(model.getFirst_Name()+" selected")
                                .setIcon(R.drawable.ic_person_add)
                                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                        Toast.makeText(getContext(), "Added", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(context, CheckOut.class);
                                        intent.putExtra("contact_id",contact_id);
                                        intent.putExtra("test",test);
                                        context.startActivity(intent);
                                        dismiss();

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                       dialog.dismiss();
                                    }
                                })
                                .show();
                        dismiss();

                    }
                });
                //progressBar exit
            }
        };


        recyclerView.setAdapter(adapter);
        return rootView;
    }


    public class ContactsViewHolder extends RecyclerView.ViewHolder {
        public TextView name, phone, email, image_letter,zip;
        public CircleImageView image;
        public RelativeLayout lyt_checked, lyt_image;
        public View lyt_parent;
        public ContactsViewHolder(View view) {
            super(view);

            name =  view.findViewById(R.id.fname);
            email =  view.findViewById(R.id.email);
            phone =  view.findViewById(R.id.phone);
            zip =  view.findViewById(R.id.zip);
            image_letter =  view.findViewById(R.id.image_letter);
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null){
            adapter.stopListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null){
            adapter.stopListening();
        }
    }
}