package com.pos.getplusx.Activities.Preview_Checkout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pos.getplusx.R;

public class CheckOut extends AppCompatActivity {

    private EditText  amount_pay;
    private TextView amount;
    private Button btn;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        SharedPreferences preferences = getSharedPreferences("total",MODE_PRIVATE);
        final int sum = preferences.getInt("sum",0);
        String itemId = preferences.getString("itemId","default");

        String contactId = getIntent().getStringExtra("contact_id");
        String test = getIntent().getStringExtra("test");

        Toast.makeText(this,itemId+test , Toast.LENGTH_SHORT).show();


        amount = findViewById(R.id.amount_);
        amount_pay = findViewById(R.id.amount_pay);
        btn = findViewById(R.id.charge);

        amount.setText(String.valueOf(sum));
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (amount_pay == null){
//                    amount_pay.setError("Input amount");
//
//                }
//                else{
//                    float change =0;
//                     change = Integer.parseInt(amount_pay.getText().toString())- sum;
//                    Intent intent = new Intent(CheckOut.this,Receipt.class);
//                    intent.putExtra("change",change);
//                    intent.putExtra("sum",sum);
//                    intent.putExtra("cash",amount_pay.getText().toString());
//                    startActivity(intent);
//                }
//            }
//        });
    }
}
