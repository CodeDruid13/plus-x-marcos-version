package com.pos.getplusx.Activities.Shops;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pos.getplusx.Activities.Dashboard.MainActivity;
import com.pos.getplusx.R;
import com.pos.getplusx.model.stores;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import spencerstudios.com.bungeelib.Bungee;

import static android.widget.LinearLayout.VERTICAL;

public class ShopsActivity extends AppCompatActivity {

    FirebaseRecyclerAdapter adapter;
    DatabaseReference myref;
    private ProgressBar loading;
    FloatingActionButton refresh;
    TextView td;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops);


        loading = findViewById(R.id.loading);
        td = findViewById(R.id.td);


//        refresh = findViewById(R.id.fab_refresh);
//        refresh.setOnClickListener(new View.OnClickListener() {
//            int Counter = 0;
//            @Override
//            public void onClick(View v) {
//                adapter.startListening();
//                Toast.makeText(ShopsActivity.this, "You are online", Toast.LENGTH_SHORT).show();
////                startActivity(new Intent(ShopsActivity.this, Customer.class));
////                Bungee.inAndOut(ShopsActivity.this)
//
//            }
//        });


        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        myref = FirebaseDatabase.getInstance().getReference().child("Businesses").child(uid);
        myref.keepSynced(true);

        RecyclerView recyclerView = findViewById(R.id.main_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(ShopsActivity.this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration decoration = new DividerItemDecoration(ShopsActivity.this, VERTICAL);
        recyclerView.addItemDecoration(decoration);

        FirebaseRecyclerOptions<stores> options =
                new FirebaseRecyclerOptions.Builder<stores>()
                        .setQuery(myref, stores.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<stores, StoresViewHolder>(options) {
            @NonNull
            @Override
            public StoresViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View s = LayoutInflater.from(ShopsActivity.this)
                        .inflate(R.layout.item_store, parent, false);

                return new StoresViewHolder(s);
            }

            @Override
            protected void onBindViewHolder(@NonNull StoresViewHolder holder, int position, @NonNull stores model) {
                final String shopId = getRef(position).getKey();
                holder.name.setText(model.getBusiness_name());
                holder.location.setText(model.getCity());
                //holder.currency.setText(model.getCurrency());
                holder.town.setText(model.getAddress());
                holder.setImageView(model.getLogo());


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(ShopsActivity.this, R.style.MyDialogTheme)
                                .setTitle("Select Shop?")
                                .setMessage("Select this shop to proceed")
                                .setIcon(R.drawable.ic_info_outline)
                                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        assert shopId != null;
                                        finish();
                                        Intent intent = new Intent(ShopsActivity.this,MainActivity.class);
                                        intent.putExtra("id",shopId);
                                        startActivity(intent);
                                        Bungee.fade(ShopsActivity.this);

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }
                });
            }
        };

        loading.setVisibility(View.GONE);

        recyclerView.setAdapter(adapter);
    }

    private class StoresViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView currency,location,name,town;
        ImageView logo;
        public StoresViewHolder(View itemView) {
            super(itemView);

            mView=itemView;
            location  = mView.findViewById(R.id.location);
            name  = mView.findViewById(R.id.bs_name);
            town  = mView.findViewById(R.id.town);
            currency  = mView.findViewById(R.id.currency);
            logo  = mView.findViewById(R.id.store_logo);
        }
        public void setImageView(String image)
        {
            Picasso.get().load(image).into(logo);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null){
            adapter.stopListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null){
            adapter.stopListening();
        }
    }
}
